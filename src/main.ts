import { NestFactory, Reflector } from '@nestjs/core';

import { ValidationPipe } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';

import { AppModule } from './app.module';
import { AuthGuard } from './common/guards/auth.guard';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  const reflector = app.get(Reflector);
  const jwtService = app.get(JwtService);
  const configService = app.get(ConfigService);

  const authGuard = new AuthGuard(reflector, jwtService, configService);
  const appPort = configService.get<string>('APP_PORT')!;

  app.useGlobalPipes(new ValidationPipe({ transform: true }));
  app.useGlobalGuards(authGuard);

  app.enableCors();
  app.setGlobalPrefix('api/v1');

  await app.listen(appPort);
}

bootstrap().catch(console.error);
