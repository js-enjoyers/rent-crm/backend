type DefaultQueryOptions = {
  term?: string;
  limit?: number;
  offset?: number;
  withArchived?: boolean;
};
