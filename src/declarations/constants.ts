/**
 * Users related constants
 */
export const MIN_USER_LOGIN_LENGTH = 3;
export const MAX_USER_LOGIN_LENGTH = 32;
export const USER_PIN_LENGTH = 4;

/**
 * Amenities related constants
 */
export const MIN_AMENITY_NAME_LENGTH = 3;
export const MAX_AMENITY_NAME_LENGTH = 64;

/**
 * Properties related values
 */
export const MIN_PROPERTY_NAME_LENGTH = 3;
export const MAX_PROPERTY_NAME_LENGTH = 64;
export const MIN_PROPERTY_CAPACITY = 1;

/**
 * Rates related values
 */
export const MIN_RATE_NAME_LENGTH = 3;
export const MAX_RATE_NAME_LENGTH = 32;

/**
 * Guests related values
 */
export const MIN_GUEST_PHONE_LENGTH = 11;
export const MAX_GUEST_PHONE_LENGTH = 11;
export const MIN_GUEST_NOTE_LENGTH = 3;
export const MAX_GUEST_NOTE_LENGTH = 64;
export const MIN_GUEST_NAME_LENGTH = 3;
export const MAX_GUEST_NAME_LENGTH = 48;

/**
 * Booking related values
 */
export const MAX_BOOKING_COMMENT_LENGTH = 255;
export const MIN_BOOKING_CORRECTION_REASON_LENGTH = 3;
export const MAX_BOOKING_CORRECTION_REASON_LENGTH = 64;
