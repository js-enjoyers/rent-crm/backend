import { Global, Module } from '@nestjs/common';

import { JwtModule } from '@nestjs/jwt';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModule, TypeOrmModuleOptions } from '@nestjs/typeorm';

import { typeormConfig } from './typeorm.config';

import { UsersModule } from './features/users/users.module';
import { AuthModule } from './features/auth/auth.module';
import { AmenitiesModule } from './features/amenities/amenities.module';
import { PropertiesModule } from './features/properties/properties.module';
import { RatesModule } from './features/rates/rates.module';
import { GuestsModule } from './features/guests/guests.module';
import { BookingsModule } from './features/bookings/bookings.module';

import { TransactionFactory } from './common/services/transaction-factory';
import { DashboardModule } from './features/dashboard/dashboard.module';

@Global()
@Module({
  imports: [
    ConfigModule.forRoot({ isGlobal: true, load: [typeormConfig] }),
    JwtModule.register({ global: true }),
    TypeOrmModule.forRootAsync({
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => {
        return configService.get<TypeOrmModuleOptions>('typeorm')!;
      },
    }),
    UsersModule,
    AuthModule,
    AmenitiesModule,
    PropertiesModule,
    RatesModule,
    GuestsModule,
    BookingsModule,
    DashboardModule,
  ],
  providers: [TransactionFactory],
  exports: [TransactionFactory],
})
export class AppModule {}
