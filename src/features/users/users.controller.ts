import {
  BadRequestException,
  NotFoundException,
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Post,
} from '@nestjs/common';

import { UsersService } from './users.service';
import { CredentialsDto } from '../../common/dto/credentials.dto';

import { applyOmit } from '../../utils';

@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  /**
   * Pagination seems redundant here since we'd have only
   * few users in production
   */
  @Get()
  public get() {
    return this.usersService.get();
  }

  @Post()
  public async create(@Body() payload: CredentialsDto) {
    const user = await this.usersService.getOne({
      login: payload.login,
    });

    if (user) {
      throw new BadRequestException('Аккаунт с таким именем уже существует');
    }

    const result = await this.usersService.create(payload);
    return applyOmit(result!, ['pin']);
  }

  @Delete(':id')
  public async remove(@Param('id', ParseIntPipe) id: number) {
    const user = await this.usersService.getOne({ id });

    if (!user) {
      throw new NotFoundException('Аккаунт не найден');
    }

    const usersTotal = await this.usersService.getTotalCount();

    if (usersTotal === 1) {
      throw new BadRequestException(
        'Невозможно удалить последнего пользователя',
      );
    }

    await this.usersService.remove(id);

    return applyOmit(user, ['pin']);
  }
}
