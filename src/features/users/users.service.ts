import { hash } from 'bcrypt';

import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ILike, Repository } from 'typeorm';

import { User } from '../../entities/user.entity';

type GetOneOptions = {
  caseSensitive: boolean;
};

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private readonly usersRepository: Repository<User>,
  ) {}

  public get() {
    return this.usersRepository.find({
      select: ['id', 'login', 'createdAt', 'updatedAt'],
      order: { createdAt: 'DESC' },
    });
  }

  public getOne(
    { id, login }: Partial<Pick<User, 'id' | 'login'>>,
    { caseSensitive }: Partial<GetOneOptions> = {},
  ) {
    const whereOptions = [];

    if (id) {
      whereOptions.push({ id });
    }

    if (login) {
      /**
       * Case-sensitive parameter is important,
       * since this method is used on auth validation
       */
      whereOptions.push({
        login: caseSensitive ? login : ILike(login),
      });
    }

    return this.usersRepository.findOne({
      where: whereOptions,
    });
  }

  public getTotalCount() {
    return this.usersRepository.count();
  }

  public async create({ login, pin }: Pick<User, 'login' | 'pin'>) {
    const pinHash = await hash(pin, 8);

    const {
      identifiers: [{ id }],
    } = await this.usersRepository.insert({
      pin: pinHash,
      login,
    });

    return this.getOne({ id });
  }

  public remove(id: User['id']) {
    return this.usersRepository.delete({ id });
  }
}
