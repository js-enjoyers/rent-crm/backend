import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { BookingsController } from './bookings.controller';
import { BookingsService } from './bookings.service';

import { Booking } from '../../entities/booking.entity';
import { BookingAmenity } from '../../entities/booking-amenity.entity';
import { Correction } from '../../entities/correction.entity';

import { PropertiesModule } from '../properties/properties.module';
import { RatesModule } from '../rates/rates.module';
import { GuestsModule } from '../guests/guests.module';
import { AmenitiesModule } from '../amenities/amenities.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([Booking, BookingAmenity, Correction]),
    PropertiesModule,
    RatesModule,
    GuestsModule,
    AmenitiesModule,
  ],
  providers: [BookingsService],
  controllers: [BookingsController],
})
export class BookingsModule {}
