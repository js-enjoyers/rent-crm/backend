import {
  BadRequestException,
  Body,
  Controller,
  DefaultValuePipe,
  Get,
  NotFoundException,
  Param,
  ParseIntPipe,
  Patch,
  Post,
  Query,
} from '@nestjs/common';

import { BookingsService } from './bookings.service';
import { BookingAmenityDto, CreateBookingDto } from './dto/create-booking.dto';

import { PropertiesService } from '../properties/properties.service';
import { GuestsService } from '../guests/guests.service';
import { RatesService } from '../rates/rates.service';

import { AmenitiesService } from '../amenities/amenities.service';
import { Amenity } from '../../entities/amenity.entity';

import { ParseDatePipe } from '../../common/pipes';
import { BookingStatus } from '../../declarations';

import {
  UpdateBookingAmenitiesDto,
  UpdateBookingCommentDto,
  UpdateBookingCorrectionsDto,
  UpdateBookingDatesDto,
  UpdateBookingGuestsCountDto,
  UpdateBookingStatusDto,
} from './dto/update-booking.dto';

type AmenityWithQuantity = Amenity & { quantity: number };

@Controller('bookings')
export class BookingsController {
  constructor(
    private readonly bookingsService: BookingsService,
    private readonly propertiesService: PropertiesService,
    private readonly ratesService: RatesService,
    private readonly guestsService: GuestsService,
    private readonly amenitiesService: AmenitiesService,
  ) {}

  @Get()
  public get(
    @Query('limit', new DefaultValuePipe(20), ParseIntPipe) limit?: number,
    @Query('offset', new DefaultValuePipe(0), ParseIntPipe) offset?: number,
    @Query('start', ParseDatePipe) start?: Date,
    @Query('end', ParseDatePipe) end?: Date,
    @Query('propertyId') propertyId?: number,
    @Query('guestId') guestId?: number,
    @Query('status') status?: BookingStatus,
  ) {
    return this.bookingsService.get({
      limit,
      offset,
      propertyId,
      guestId,
      status,
      ...(start && { start }),
      ...(end && { end }),
    });
  }

  @Post()
  public async create(@Body() payload: CreateBookingDto) {
    const [property, rate, guest] = await Promise.all([
      this.propertiesService.getOne({ id: payload.propertyId }),
      this.ratesService.getOne({ id: payload.rateId }),
      this.guestsService.getOne({ id: payload.guestId }),
    ]);

    if (!property) throw new BadRequestException('Объект не найден');
    if (!rate) throw new BadRequestException('Тариф не найден');
    if (!guest) throw new BadRequestException('Гость не найден');

    if (property.isArchived()) throw new BadRequestException('Объект в архиве');
    if (rate.isArchived()) throw new BadRequestException('Тариф в архиве');

    const bookingsInRange = await this.bookingsService.getInRange({
      from: payload.startDate,
      to: payload.endDate,
      property,
    });

    if (bookingsInRange.length) {
      throw new BadRequestException(
        'Выбранные даты недоступны для бронирования',
      );
    }

    const amenities = await this.validateAmenitiesList(payload.amenities);

    return this.bookingsService.create({
      data: payload,
      amenities,
      rate,
    });
  }

  @Get('range')
  public async getInRange(
    @Query('start', ParseDatePipe) start: Date,
    @Query('end', ParseDatePipe) end: Date,
    @Query('propertyId', new DefaultValuePipe(0), ParseIntPipe)
      propertyId: number, // eslint-disable-line -- Fixes prettier conflict
  ) {
    if (!propertyId) {
      return this.bookingsService.getInRange({ from: start, to: end });
    }

    const property = await this.propertiesService.getOne({ id: propertyId });

    if (!property) {
      throw new BadRequestException('Объект не найден');
    }

    return this.bookingsService.getInRange({
      from: start,
      to: end,
      property,
    });
  }

  @Get(':id')
  public async getOne(@Param('id', ParseIntPipe) id: number) {
    const booking = await this.bookingsService.getOne(id);

    if (!booking) {
      throw new NotFoundException('Бронирование не найдено');
    }

    return booking;
  }

  @Patch(':id/status')
  public async setStatus(
    @Param('id', ParseIntPipe) id: number,
    @Body() { status: targetStatus }: UpdateBookingStatusDto,
  ) {
    const booking = await this.bookingsService.getOne(id);

    if (!booking) {
      throw new NotFoundException('Бронирование не найдено');
    }

    if (targetStatus === BookingStatus.BOOKED) {
      throw new BadRequestException('Этот статус нельзя установить вручную');
    }

    if (targetStatus === BookingStatus.ACTIVE && !booking.isBooked()) {
      throw new BadRequestException(
        'Невозможно активировать бронирование повторно',
      );
    }

    if (targetStatus === BookingStatus.DONE && !booking.isActive()) {
      throw new BadRequestException(
        'Завершить можно только активное бронирование',
      );
    }

    if (targetStatus === BookingStatus.CANCELLED && !booking.isBooked()) {
      throw new BadRequestException('Отменить бронирование уже невозможно');
    }

    return this.bookingsService.setStatus(id, targetStatus);
  }

  @Patch(':id/comment')
  public async updateComment(
    @Param('id', ParseIntPipe) id: number,
    @Body() { comment }: UpdateBookingCommentDto,
  ) {
    const booking = await this.bookingsService.getOne(id);

    if (!booking) {
      throw new NotFoundException('Бронирование не найдено');
    }

    return this.bookingsService.updateComment(id, comment);
  }

  @Patch(':id/guests/count')
  public async updateGuestsCount(
    @Param('id', ParseIntPipe) id: number,
    @Body() { adults, children }: UpdateBookingGuestsCountDto,
  ) {
    const booking = await this.bookingsService.getOne(id);

    if (!booking) {
      throw new NotFoundException('Бронирование не найдено');
    }

    if (!booking.isBooked()) {
      throw new BadRequestException(
        'На данном этапе бронирования изменить количество гостей невозможно',
      );
    }

    return this.bookingsService.updateGuestsCount(id, { adults, children });
  }

  @Patch(':id/amenities')
  public async updateAmenities(
    @Param('id', ParseIntPipe) id: number,
    @Body() { amenities }: UpdateBookingAmenitiesDto,
  ) {
    const targetAmenities = await this.validateAmenitiesList(amenities);
    const booking = await this.bookingsService.getOne(id);

    if (!booking) {
      throw new NotFoundException('Бронирование не найдено');
    }

    if (booking.isDone() || booking.isCancelled()) {
      throw new BadRequestException(
        'На данном этапе жизненного цикла изменить услуги невозможно',
      );
    }

    return this.bookingsService.updateAmenities(id, targetAmenities);
  }

  @Patch(':id/corrections')
  public async updateCorrections(
    @Param('id', ParseIntPipe) id: number,
    @Body() { corrections }: UpdateBookingCorrectionsDto,
  ) {
    const booking = await this.bookingsService.getOne(id);

    if (!booking) {
      throw new NotFoundException('Бронирование не найдено');
    }

    if (booking.isDone() || booking.isCancelled()) {
      throw new BadRequestException(
        'На данном этапе жизненного цикла изменить коррекции невозможно',
      );
    }

    return this.bookingsService.updateCorrections(id, corrections);
  }

  @Patch(':id/dates')
  public async updateDates(
    @Param('id', ParseIntPipe) id: number,
    @Body() { endDate }: UpdateBookingDatesDto,
  ) {
    const booking = await this.bookingsService.getOne(id);

    if (!booking) {
      throw new NotFoundException('Бронирование не найдено');
    }

    if (booking.isDone() || booking.isCancelled()) {
      throw new BadRequestException(
        'На данном этапе жизненного цикла изменить даты невозможно',
      );
    }

    const isRangeValid =
      Number(new Date(endDate)) > Number(new Date(booking.startDate));

    if (!isRangeValid) {
      throw new BadRequestException(
        'Конечная дата не может быть меньше начальной',
      );
    }

    const bookingsInRange = await this.bookingsService
      .getInRange({
        from: new Date(booking.startDate),
        to: endDate,
        property: booking.property,
      })
      .then((bookings) => {
        return bookings.filter((booking) => booking.id !== id);
      });

    if (bookingsInRange.length) {
      throw new BadRequestException(
        'Выбранные даты недоступны для бронирования',
      );
    }

    return this.bookingsService.updateDates(id, { endDate });
  }

  private async validateAmenitiesList(
    amenities: BookingAmenityDto[],
  ): Promise<AmenityWithQuantity[]> {
    const amenitiesIds = amenities.map(({ amenityId }) => amenityId);
    const targetAmenities = await this.amenitiesService.getList(amenitiesIds);

    if (amenitiesIds.length !== targetAmenities.length) {
      throw new BadRequestException('Одна или несколько услуг не существуют');
    }

    const isAnyArchived = targetAmenities.some((amenity) => {
      return amenity.isArchived();
    });

    if (isAnyArchived) {
      throw new BadRequestException(
        'Одна или несколько услуг находятся в архиве',
      );
    }

    return targetAmenities.map<AmenityWithQuantity>((item) => {
      const correspondingDto = amenities.find((dto) => {
        return dto.amenityId === item.id;
      })!;

      return {
        ...item,
        quantity: correspondingDto.quantity,
      } as AmenityWithQuantity;
    });
  }
}
