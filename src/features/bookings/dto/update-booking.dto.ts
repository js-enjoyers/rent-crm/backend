import { Transform, Type } from 'class-transformer';

import {
  IsArray,
  IsDate,
  IsEnum,
  IsNumber,
  IsOptional,
  IsString,
  Length,
  Min,
  ValidateNested,
} from 'class-validator';

import {
  BookingStatus,
  MAX_BOOKING_COMMENT_LENGTH,
} from '../../../declarations';

import { BookingAmenityDto, ProfitCorrectionDto } from './create-booking.dto';

export class UpdateBookingStatusDto {
  @IsEnum(BookingStatus)
  public status: BookingStatus;
}

export class UpdateBookingCommentDto {
  @IsOptional()
  @IsString()
  @Length(1, MAX_BOOKING_COMMENT_LENGTH)
  public comment: string | null = null;
}

export class UpdateBookingGuestsCountDto {
  @IsNumber()
  @Min(1)
  public adults: number;

  @IsNumber()
  @Min(0)
  public children: number;
}

export class UpdateBookingAmenitiesDto {
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => BookingAmenityDto)
  public amenities: BookingAmenityDto[];
}

export class UpdateBookingCorrectionsDto {
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => ProfitCorrectionDto)
  public corrections: ProfitCorrectionDto[];
}

export class UpdateBookingDatesDto {
  @Transform((params) => {
    return new Date(new Date(params.value).toUTCString());
  })
  @IsDate()
  public endDate: Date;
}
