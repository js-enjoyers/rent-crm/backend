import { Transform, Type } from 'class-transformer';
import {
  IsArray,
  IsDate,
  IsDefined,
  IsNumber,
  IsOptional,
  IsPositive,
  IsString,
  Length,
  Min,
  Validate,
  ValidateNested,
} from 'class-validator';

import {
  MAX_BOOKING_COMMENT_LENGTH,
  MIN_BOOKING_CORRECTION_REASON_LENGTH,
  MAX_BOOKING_CORRECTION_REASON_LENGTH,
} from '../../../declarations';

import { IsNotZero } from '../../../common/validators/is-not-zero.validator';

export class BookingAmenityDto {
  @IsDefined()
  @IsNumber()
  public amenityId: number;

  @IsNumber()
  @IsPositive()
  public quantity: number;
}

export class ProfitCorrectionDto {
  @IsDefined()
  @IsNumber()
  @Validate(IsNotZero)
  public value: number;

  @IsDefined()
  @IsString()
  @Length(
    MIN_BOOKING_CORRECTION_REASON_LENGTH,
    MAX_BOOKING_CORRECTION_REASON_LENGTH,
  )
  public reason: string;
}

export class CreateBookingDto {
  @IsDefined()
  @IsNumber()
  public propertyId: number;

  @Transform((params) => {
    return new Date(new Date(params.value).toUTCString());
  })
  @IsDate()
  public startDate: Date;

  @Transform((params) => {
    return new Date(new Date(params.value).toUTCString());
  })
  @IsDate()
  public endDate: Date;

  @IsDefined()
  @IsNumber()
  public rateId: number;

  @IsDefined()
  @IsNumber()
  public guestId: number;

  @IsNumber()
  @Min(1)
  public adults: number;

  @IsNumber()
  @Min(0)
  public children: number;

  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => BookingAmenityDto)
  public amenities: BookingAmenityDto[];

  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => ProfitCorrectionDto)
  public corrections: ProfitCorrectionDto[];

  @Transform((params) => params.value?.trim())
  @Length(1, MAX_BOOKING_COMMENT_LENGTH)
  @IsOptional()
  public comment?: string;
}
