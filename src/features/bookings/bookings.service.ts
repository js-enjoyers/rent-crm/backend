import { DateTime } from 'luxon';

import { Injectable, InternalServerErrorException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Brackets, FindManyOptions, FindOneOptions, Repository } from 'typeorm';

import { Booking } from '../../entities/booking.entity';
import { BookingAmenity } from '../../entities/booking-amenity.entity';

import {
  Transaction,
  TransactionFactory,
} from '../../common/services/transaction-factory';

import { Property } from '../../entities/property.entity';
import { Amenity } from '../../entities/amenity.entity';
import { Rate } from '../../entities/rate.entity';
import { Correction } from '../../entities/correction.entity';

import { BookingStatus } from '../../declarations';

import {
  CreateBookingDto,
  ProfitCorrectionDto,
} from './dto/create-booking.dto';

type GetInRangeOptions = {
  from: Date;
  to: Date;
  property?: Property;
};

type ExtendedAmenity = Amenity & {
  quantity: number;
  appliedProfit: string | number;
};

type ExtendedBooking = Booking & {
  amenities: ExtendedAmenity[];
};

type CreateBookingPayload = {
  data: CreateBookingDto;
  amenities: Omit<ExtendedAmenity, 'appliedProfit'>[];
  rate: Rate;
};

type GetBookingsOptions = DefaultQueryOptions &
  Partial<{
    start: Date;
    end: Date;
    propertyId: number;
    guestId: number;
    status: BookingStatus;
  }>;

@Injectable()
export class BookingsService {
  constructor(
    private readonly transactionFactory: TransactionFactory,
    @InjectRepository(Booking)
    private readonly bookingsRepository: Repository<Booking>,
    @InjectRepository(BookingAmenity)
    private readonly bookingsAmenitiesRepository: Repository<BookingAmenity>,
  ) {}

  public get(options: GetBookingsOptions) {
    const query = this.bookingsRepository.createQueryBuilder('booking');

    query.leftJoinAndSelect('booking.property', 'property');
    query.leftJoinAndSelect('booking.guest', 'guest');
    query.leftJoinAndSelect('guest.phones', 'phones');
    query.leftJoinAndSelect('guest.notes', 'notes');

    if (options.status) {
      query.andWhere('booking.status = :status', { status: options.status });
    }

    if (options.propertyId) {
      query.andWhere('booking.propertyId = :propertyId', {
        propertyId: options.propertyId,
      });
    }

    if (options.guestId) {
      query.andWhere('booking.guestId = :guestId', {
        guestId: options.guestId,
      });
    }

    if (options.start && options.end) {
      const whereParams = {
        from: options.start.toUTCString(),
        to: options.end.toUTCString(),
      };

      query.andWhere(
        new Brackets((qb) => {
          qb.where('booking.startDate BETWEEN :from AND :to', whereParams)
            .orWhere('booking.endDate BETWEEN :from AND :to', whereParams)
            .orWhere(
              new Brackets((subQb) => {
                subQb.where(
                  'booking.startDate <= :from AND booking.endDate >= :to',
                  whereParams,
                );
              }),
            );
        }),
      );
    }

    query.orderBy('booking.startDate', 'DESC');

    query.take(options.limit);
    query.skip(options.offset);

    return query.getMany();
  }

  public async getInRange({ from, to, property }: GetInRangeOptions) {
    const query = this.bookingsRepository.createQueryBuilder('booking');

    const whereParams = {
      from: from.toUTCString(),
      to: to.toUTCString(),
    };

    query.where(
      new Brackets((qb) => {
        qb.where('booking.startDate BETWEEN :from AND :to', whereParams)
          .orWhere('booking.endDate BETWEEN :from AND :to', whereParams)
          .orWhere(
            new Brackets((subQb) => {
              subQb.where(
                'booking.startDate <= :from AND booking.endDate >= :to',
                whereParams,
              );
            }),
          );
      }),
    );

    query.andWhere('booking.status != :status', {
      status: BookingStatus.CANCELLED,
    });

    query.leftJoinAndSelect('booking.property', 'property');
    query.leftJoinAndSelect('booking.rate', 'rate');
    query.leftJoinAndSelect('booking.corrections', 'corrections');

    query.leftJoinAndSelect('booking.guest', 'guest');
    query.leftJoinAndSelect('guest.phones', 'phones');
    query.leftJoinAndSelect('guest.notes', 'notes');

    if (property) {
      query.andWhere('booking.propertyId = :propertyId', {
        propertyId: property.id,
      });
    }

    query.orderBy('booking.startDate', 'DESC');

    const bookings = await query.getMany();

    return Promise.all(
      bookings.map(async (booking) => {
        const amenities = await this.getRelatedAmenities(booking.id);

        return Object.assign(booking, { amenities });
      }),
    );
  }

  public async getOne(id: Booking['id'], transaction?: Transaction) {
    const findOptions: FindOneOptions<Booking> = {
      where: { id },
      relations: [
        'property',
        'corrections',
        'rate',
        'guest',
        'guest.phones',
        'guest.notes',
      ],
      withDeleted: true,
    };

    const booking = transaction
      ? await transaction.manager.findOne(Booking, findOptions)
      : await this.bookingsRepository.findOne(findOptions);

    if (!booking) {
      return null;
    }

    const amenities = await this.getRelatedAmenities(id, transaction);
    return Object.assign(booking, { amenities });
  }

  public async create({ data, amenities, rate }: CreateBookingPayload) {
    const transaction = await this.transactionFactory.create();

    try {
      const {
        identifiers: [{ id: bookingId }],
      } = await transaction.manager.insert(Booking, {
        status: BookingStatus.BOOKED,
        totalProfit: '0.00',
        appliedRateProfit: rate.profit,
        startDate: data.startDate.toUTCString(),
        endDate: data.endDate.toUTCString(),
        adults: data.adults,
        children: data.children,
        propertyId: data.propertyId,
        rateId: data.rateId,
        guestId: data.guestId,
        ...(data.comment && { comment: data.comment }),
      });

      const newCorrections = data.corrections.map((correction) => {
        return { ...correction, bookingId };
      });

      const newBookingsAmenities = amenities.map((amenity) => {
        return {
          appliedProfit: amenity.profit,
          quantity: amenity.quantity,
          amenityId: amenity.id,
          bookingId,
        };
      });

      await Promise.all([
        transaction.manager.insert(Correction, newCorrections),
        transaction.manager.insert(BookingAmenity, newBookingsAmenities),
      ]);

      const createdBooking = (await this.getOne(bookingId, transaction))!;
      const totalProfit = await this.recalculateTotalProfit(
        createdBooking,
        transaction,
      );

      await transaction.commit();

      createdBooking.totalProfit = totalProfit;
      return createdBooking;
    } catch (error) {
      console.error(error);
      await transaction.rollback();

      throw new InternalServerErrorException(
        'Возникла ошибка при обновлении бронирования',
      );
    }
  }

  public async setStatus(id: Booking['id'], status: BookingStatus) {
    await this.bookingsRepository.update({ id }, { status });
    return { status };
  }

  public async updateComment(id: Booking['id'], comment: string | null) {
    await this.bookingsRepository.update({ id }, { comment });
    return { comment };
  }

  public async updateGuestsCount(
    id: Booking['id'],
    { adults, children }: Pick<Booking, 'adults' | 'children'>,
  ) {
    const transaction = await this.transactionFactory.create();

    try {
      await transaction.manager.update(Booking, { id }, { adults, children });

      const booking = (await this.getOne(id, transaction))!;
      const totalProfit = await this.recalculateTotalProfit(
        booking,
        transaction,
      );

      await transaction.commit();

      return { adults, children, totalProfit };
    } catch (error) {
      console.error(error);
      await transaction.rollback();

      throw new InternalServerErrorException(
        'Возникла ошибка при обновлении бронирования',
      );
    }
  }

  public async updateAmenities(
    id: Booking['id'],
    amenities: Omit<ExtendedAmenity, 'appliedProfit'>[],
  ) {
    const transaction = await this.transactionFactory.create();

    const newBookingsAmenities = amenities.map((amenity) => {
      return {
        appliedProfit: amenity.profit,
        quantity: amenity.quantity,
        amenityId: amenity.id,
        bookingId: id,
      };
    });

    try {
      await transaction.manager.delete(BookingAmenity, { bookingId: id });
      await transaction.manager.insert(BookingAmenity, newBookingsAmenities);

      const booking = (await this.getOne(id, transaction))!;
      const totalProfit = await this.recalculateTotalProfit(
        booking,
        transaction,
      );

      await transaction.commit();

      return { amenities: booking.amenities, totalProfit };
    } catch (error) {
      console.error(error);
      await transaction.rollback();

      throw new InternalServerErrorException(
        'Возникла ошибка при обновлении бронирования',
      );
    }
  }

  public async updateCorrections(
    id: Booking['id'],
    corrections: ProfitCorrectionDto[],
  ) {
    const transaction = await this.transactionFactory.create();

    const newCorrections = corrections.map((data) => {
      return {
        value: data.value,
        reason: data.reason,
        bookingId: id,
      };
    });

    try {
      await transaction.manager.delete(Correction, { bookingId: id });
      await transaction.manager.insert(Correction, newCorrections);

      const booking = (await this.getOne(id, transaction))!;
      const totalProfit = await this.recalculateTotalProfit(
        booking,
        transaction,
      );

      await transaction.commit();

      return { corrections: booking.corrections, totalProfit };
    } catch (error) {
      console.error(error);
      await transaction.rollback();

      throw new InternalServerErrorException(
        'Возникла ошибка при обновлении бронирования',
      );
    }
  }

  public async updateDates(id: Booking['id'], dates: { endDate: Date }) {
    const transaction = await this.transactionFactory.create();

    try {
      const endDate = dates.endDate.toUTCString();
      await transaction.manager.update(Booking, { id }, { endDate });

      const booking = (await this.getOne(id, transaction))!;
      const totalProfit = await this.recalculateTotalProfit(
        booking,
        transaction,
      );

      await transaction.commit();

      return { endDate, totalProfit };
    } catch (error) {
      console.error(error);
      await transaction.rollback();

      throw new InternalServerErrorException(
        'Возникла ошибка при обновлении бронирования',
      );
    }
  }

  public async recalculateTotalProfit(
    booking: ExtendedBooking,
    transaction?: Transaction,
  ) {
    const nightsCount = this.calculateNightsCount(
      booking.startDate,
      booking.endDate,
    );

    const amenitiesTotalProfit = booking.amenities.reduce((result, amenity) => {
      return result + Number(amenity.appliedProfit) * amenity.quantity;
    }, 0);

    const correctionsTotalProfit = booking.corrections.reduce(
      (result, correction) => {
        return result + Number(correction.value);
      },
      0,
    );

    const totalProfit =
      Number(booking.appliedRateProfit) * booking.adults * nightsCount +
      amenitiesTotalProfit +
      correctionsTotalProfit;

    if (transaction) {
      await transaction.manager.update(
        Booking,
        { id: booking.id },
        { totalProfit },
      );

      return totalProfit;
    }

    await this.bookingsRepository.update({ id: booking.id }, { totalProfit });
    return totalProfit;
  }

  public async getRelatedAmenities(
    bookingId: Booking['id'],
    transaction?: Transaction,
  ): Promise<ExtendedAmenity[]> {
    const findOptions: FindManyOptions<BookingAmenity> = {
      where: { bookingId },
      relations: ['amenity'],
      withDeleted: true,
    };

    const bookingsAmenities = transaction
      ? await transaction.manager.find(BookingAmenity, findOptions)
      : await this.bookingsAmenitiesRepository.find(findOptions);

    return bookingsAmenities.map((item) => {
      const result = item.amenity as ExtendedAmenity;

      result.appliedProfit = item.appliedProfit;
      result.quantity = item.quantity;

      return result;
    });
  }

  private calculateNightsCount(start: string, end: string) {
    const startDate = DateTime.fromJSDate(new Date(start)).toUTC();
    const endDate = DateTime.fromJSDate(new Date(end)).toUTC();
    const nightsCount = endDate.diff(startDate, 'days').days;

    return Math.ceil(nightsCount);
  }
}
