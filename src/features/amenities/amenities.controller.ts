import {
  BadRequestException,
  Body,
  Controller,
  DefaultValuePipe,
  Delete,
  Get,
  NotFoundException,
  Param,
  ParseBoolPipe,
  ParseIntPipe,
  Post,
  Put,
  Query,
} from '@nestjs/common';

import { AmenitiesService } from './amenities.service';

import { CreateAmenityDto } from './dto/create-amenity.dto';
import { UpdateAmenityDto } from './dto/update-amenity.dto';

import { TrimPipe } from '../../common/pipes';

@Controller('amenities')
export class AmenitiesController {
  constructor(private readonly amenitiesService: AmenitiesService) {}

  @Get()
  public get(
    @Query('term', new TrimPipe()) term?: string,
    @Query('limit', new DefaultValuePipe(20), ParseIntPipe) limit?: number,
    @Query('offset', new DefaultValuePipe(0), ParseIntPipe) offset?: number,
    @Query('archive', new DefaultValuePipe(false), ParseBoolPipe)
      withArchived?: boolean, // eslint-disable-line -- Fixes prettier conflict
  ) {
    return this.amenitiesService.get({ term, limit, offset, withArchived });
  }

  @Post()
  public async create(@Body() payload: CreateAmenityDto) {
    const amenity = await this.amenitiesService.getOne({
      name: payload.name,
    });

    if (amenity) {
      throw new BadRequestException('Услуга с таким названием уже существует');
    }

    return this.amenitiesService.create(payload);
  }

  @Put(':id')
  public async update(
    @Param('id', ParseIntPipe) id: number,
    @Body() payload: UpdateAmenityDto,
  ) {
    const targetAmenity = await this.amenitiesService.getOne({ id });

    if (!targetAmenity) {
      throw new NotFoundException('Услуга не найдена');
    }

    if (targetAmenity.isArchived()) {
      throw new BadRequestException('Услуга в архиве');
    }

    const amenityByName = await this.amenitiesService.getOne({
      name: payload.name,
    });

    if (amenityByName && amenityByName.id !== id) {
      throw new BadRequestException('Услуга с таким названием уже существует');
    }

    return this.amenitiesService.update(id, payload);
  }

  @Delete(':id')
  public async archive(@Param('id', ParseIntPipe) id: number) {
    const amenity = await this.amenitiesService.getOne({ id });

    if (!amenity) {
      throw new NotFoundException('Услуга не найдена');
    }

    return this.amenitiesService.archive(id);
  }

  @Put(':id/restore')
  public async restore(@Param('id', ParseIntPipe) id: number) {
    const amenity = await this.amenitiesService.getOne({ id });

    if (!amenity) {
      throw new NotFoundException('Услуга не найдена');
    }

    return this.amenitiesService.restore(id);
  }
}
