import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindManyOptions, ILike, In, Repository } from 'typeorm';

import { Amenity } from '../../entities/amenity.entity';

@Injectable()
export class AmenitiesService {
  constructor(
    @InjectRepository(Amenity)
    private readonly amenitiesRepository: Repository<Amenity>,
  ) {}

  public get({ term, limit, offset, withArchived }: DefaultQueryOptions) {
    const findOptions: FindManyOptions<Amenity> = {
      order: { createdAt: 'DESC' },
      withDeleted: withArchived,
      take: limit,
      skip: offset,
    };

    if (term) {
      findOptions.where = { name: ILike(`%${term}%`) };
    }

    return this.amenitiesRepository.find(findOptions);
  }

  public getOne({ id, name }: Partial<Pick<Amenity, 'id' | 'name'>>) {
    const whereOptions = [];

    if (id) whereOptions.push({ id });
    if (name) whereOptions.push({ name: ILike(name) });

    return this.amenitiesRepository.findOne({
      where: whereOptions,
      withDeleted: true,
    });
  }

  public getList(ids: Amenity['id'][]) {
    return this.amenitiesRepository.find({
      where: { id: In(ids) },
      withDeleted: true,
    });
  }

  public async create(payload: Pick<Amenity, 'name' | 'profit'>) {
    const {
      identifiers: [{ id }],
    } = await this.amenitiesRepository.insert(payload);

    return this.getOne({ id });
  }

  public async update(
    id: Amenity['id'],
    payload: Pick<Amenity, 'name' | 'profit'>,
  ) {
    await this.amenitiesRepository.update({ id }, payload);
    return this.getOne({ id });
  }

  public async archive(id: Amenity['id']) {
    await this.amenitiesRepository.softDelete({ id });
    return this.getOne({ id });
  }

  public async restore(id: Amenity['id']) {
    await this.amenitiesRepository.restore({ id });
    return this.getOne({ id });
  }
}
