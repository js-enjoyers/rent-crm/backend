import { Injectable } from '@nestjs/common';

import { InjectRepository } from '@nestjs/typeorm';
import { In, Repository } from 'typeorm';

import { Guest } from '../../entities/guest.entity';
import { Note } from '../../entities/note.entity';
import { Phone } from '../../entities/phone.entity';
import { Booking } from '../../entities/booking.entity';

type PhoneCheckResult = {
  available: boolean;
  guest: Guest | null;
};

const GUEST_BOOKINGS_LIMIT = 3;

@Injectable()
export class GuestsService {
  constructor(
    @InjectRepository(Guest)
    private readonly guestsRepository: Repository<Guest>,
    @InjectRepository(Phone)
    private readonly phonesRepository: Repository<Phone>,
    @InjectRepository(Note)
    private readonly notesRepository: Repository<Note>,
    @InjectRepository(Booking)
    private readonly bookingsRepository: Repository<Booking>,
  ) {}

  /**
   * Guests-related logic
   */

  public async getOne({ id }: Pick<Guest, 'id'>) {
    const guest = await this.guestsRepository.findOne({
      where: { id },
      relations: ['notes', 'phones'],
    });

    if (!guest) {
      return null;
    }

    const bookings = await this.bookingsRepository.find({
      where: { guestId: id },
      relations: ['property'],
      take: GUEST_BOOKINGS_LIMIT,
      order: { startDate: 'DESC' },
    });

    return Object.assign(guest, { bookings });
  }

  public get({ term, limit, offset }: DefaultQueryOptions) {
    if (!term) {
      return this.guestsRepository.find({
        order: { createdAt: 'DESC' },
        relations: ['notes', 'phones'],
        take: limit,
        skip: offset,
      });
    }

    const termCondition = { term: `%${term}%` };

    return this.guestsRepository
      .createQueryBuilder('guest')
      .where('guest.name ILIKE :term', termCondition)
      .orWhere('phones.value ILIKE :term', termCondition)
      .leftJoinAndSelect('guest.phones', 'phones')
      .leftJoinAndSelect('guest.notes', 'notes')
      .orderBy('guest.createdAt', 'DESC')
      .limit(limit)
      .offset(offset)
      .getMany();
  }

  public async create(payload: Pick<Guest, 'name'>) {
    const {
      identifiers: [{ id }],
    } = await this.guestsRepository.insert(payload);

    return this.getOne({ id });
  }

  public async update(guestId: Guest['id'], payload: Pick<Guest, 'name'>) {
    await this.guestsRepository.update({ id: guestId }, payload);
    return this.getOne({ id: guestId });
  }

  /**
   * Phones-related logic
   */

  /**
   * @param id Phone identifier
   * @param value Phone number itself
   */
  public getAnyOnePhone({ id, value }: Partial<Pick<Phone, 'id' | 'value'>>) {
    return this.phonesRepository.findOne({
      where: [{ id }, { value }],
    });
  }

  public async checkPhonesAvailability(targetPhones: Phone['value'][]) {
    const conflicts = await this.phonesRepository
      .find({
        where: { value: In(targetPhones) },
        relations: ['guest'],
      })
      .then((results) => {
        return results.reduce<Record<Phone['value'], Guest>>(
          (result, phone) => {
            result[phone.value] = phone.guest;
            return result;
          },
          {},
        );
      });

    return targetPhones.map<PhoneCheckResult>((phone) => {
      return Reflect.has(conflicts, phone)
        ? { available: false, guest: conflicts[phone] }
        : { available: true, guest: null };
    });
  }

  /**
   * Used to create and assign new phone number to a guest
   *
   * @param payload.value Phone number itself
   * @param payload.force You can set this `true` to forcefully
   * overwrite already existing phone number from its owner to a
   * current guest
   */
  public async createGuestPhone(
    payload: Pick<Phone, 'value' | 'guestId'> & { force?: boolean },
  ) {
    if (payload.force) {
      await this.phonesRepository.update(
        { value: payload.value },
        { guestId: payload.guestId },
      );

      return this.getAnyOnePhone({ value: payload.value });
    }

    const {
      identifiers: [{ id }],
    } = await this.phonesRepository.insert({
      value: payload.value,
      guestId: payload.guestId,
    });

    return this.getAnyOnePhone({ id });
  }

  public async deleteGuestPhone(phoneId: Phone['id']) {
    const phone = await this.getAnyOnePhone({ id: phoneId });
    await this.phonesRepository.delete({ id: phoneId });

    return phone;
  }

  /**
   * Notes-related logic
   */

  /**
   * @param id Note identifier
   * @param text Note text itself
   */
  public getAnyOneNote({ id, text }: Partial<Pick<Note, 'id' | 'text'>>) {
    return this.notesRepository.findOne({
      where: [{ id }, { text }],
    });
  }

  public async createGuestNote(payload: Pick<Note, 'text' | 'guestId'>) {
    const {
      identifiers: [{ id }],
    } = await this.notesRepository.insert({
      text: payload.text,
      guestId: payload.guestId,
    });

    return this.getAnyOneNote({ id });
  }

  public async deleteGuestNote(noteId: Note['id']) {
    const note = await this.getAnyOneNote({ id: noteId });
    await this.notesRepository.delete({ id: noteId });

    return note;
  }
}
