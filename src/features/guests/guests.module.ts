import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { GuestsController } from './guests.controller';
import { GuestsService } from './guests.service';

import { Guest } from '../../entities/guest.entity';
import { Phone } from '../../entities/phone.entity';
import { Note } from '../../entities/note.entity';
import { Booking } from '../../entities/booking.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Guest, Phone, Note, Booking])],
  providers: [GuestsService],
  controllers: [GuestsController],
  exports: [GuestsService],
})
export class GuestsModule {}
