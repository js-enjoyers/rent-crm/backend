import {
  BadRequestException,
  Body,
  Controller,
  DefaultValuePipe,
  Delete,
  Get,
  NotFoundException,
  Param,
  ParseIntPipe,
  Post,
  Put,
  Query,
} from '@nestjs/common';

import { CheckPhonesAvailabilityDto } from './dto/check-phones-availability.dto';

import { CreateGuestDto } from './dto/create-guest.dto';
import { UpdateGuestDto } from './dto/update-guest.dto';

import { CreatePhoneDto } from './dto/create-phone.dto';
import { CreateNoteDto } from './dto/create-note.dto';

import { TrimPipe } from '../../common/pipes';
import { GuestsService } from './guests.service';

@Controller('guests')
export class GuestsController {
  constructor(private readonly guestsService: GuestsService) {}
  /**
   * Guests endpoints
   */

  @Get()
  public get(
    @Query('term', new TrimPipe()) term?: string,
    @Query('limit', new DefaultValuePipe(20), ParseIntPipe) limit?: number,
    @Query('offset', new DefaultValuePipe(0), ParseIntPipe) offset?: number,
  ) {
    return this.guestsService.get({ term, limit, offset });
  }

  @Get(':id')
  public async getOne(@Param('id', ParseIntPipe) id: number) {
    const guest = await this.guestsService.getOne({ id });

    if (!guest) {
      throw new NotFoundException('Гость не найден');
    }

    return guest;
  }

  @Post()
  public create(@Body() payload: CreateGuestDto) {
    return this.guestsService.create(payload);
  }

  @Put(':guestId')
  public async update(
    @Param('guestId', ParseIntPipe) guestId: number,
    @Body() payload: UpdateGuestDto,
  ) {
    const guest = await this.guestsService.getOne({ id: guestId });

    if (!guest) {
      throw new NotFoundException('Гость не существует');
    }

    return this.guestsService.update(guestId, payload);
  }

  /**
   * Phones endpoints
   */

  @Post('phones/check-availability')
  public checkPhonesAvailability(@Body() payload: CheckPhonesAvailabilityDto) {
    return this.guestsService.checkPhonesAvailability(payload.phones);
  }

  @Post('phones')
  public async createGuestPhone(@Body() payload: CreatePhoneDto) {
    const guest = await this.guestsService.getOne({ id: payload.guestId });

    if (!guest) {
      throw new NotFoundException('Гость не существует');
    }

    const existingPhone = await this.guestsService.getAnyOnePhone({
      value: payload.value,
    });

    if (existingPhone?.guestId === payload.guestId) {
      throw new BadRequestException('Номер уже используется этим гостем');
    }

    if (!payload.force && existingPhone) {
      throw new BadRequestException(
        `Номер ${payload.value} уже занят другим гостем`,
      );
    }

    if (payload.force && !existingPhone) {
      throw new BadRequestException(
        `Не удалось назначить гостя владельцем номера "${payload.value}"`,
      );
    }

    return this.guestsService.createGuestPhone(payload);
  }

  @Delete('phones/:phoneId')
  public async deleteGuestPhone(
    @Param('phoneId', ParseIntPipe) phoneId: number,
  ) {
    const phone = await this.guestsService.getAnyOnePhone({
      id: phoneId,
    });

    if (!phone) {
      throw new NotFoundException('Номер не существует');
    }

    return this.guestsService.deleteGuestPhone(phoneId);
  }

  /**
   * Notes endpoints
   */

  @Post('notes')
  public async createGuestNote(@Body() payload: CreateNoteDto) {
    const guest = await this.guestsService.getOne({ id: payload.guestId });

    if (!guest) {
      throw new NotFoundException('Гость не существует');
    }

    return this.guestsService.createGuestNote(payload);
  }

  @Delete('notes/:noteId')
  public async deleteGuestNote(@Param('noteId', ParseIntPipe) noteId: number) {
    const note = await this.guestsService.getAnyOneNote({
      id: noteId,
    });

    if (!note) {
      throw new NotFoundException('Заметка не существует');
    }

    return this.guestsService.deleteGuestNote(noteId);
  }
}
