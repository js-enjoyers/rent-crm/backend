import { ArrayNotEmpty, IsArray, IsString, Length } from 'class-validator';

import {
  MAX_GUEST_PHONE_LENGTH,
  MIN_GUEST_PHONE_LENGTH,
} from '../../../declarations';

export class CheckPhonesAvailabilityDto {
  @IsArray()
  @IsString({ each: true })
  @Length(MIN_GUEST_PHONE_LENGTH, MAX_GUEST_PHONE_LENGTH, { each: true })
  @ArrayNotEmpty()
  public phones: string[];
}
