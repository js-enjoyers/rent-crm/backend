import { Transform } from 'class-transformer';
import { IsNotEmpty, IsString, Length } from 'class-validator';

import {
  MIN_GUEST_NAME_LENGTH,
  MAX_GUEST_NAME_LENGTH,
} from '../../../declarations';

export class CreateGuestDto {
  @Transform((params) => params.value.trim())
  @Length(MIN_GUEST_NAME_LENGTH, MAX_GUEST_NAME_LENGTH)
  @IsNotEmpty()
  @IsString()
  public name: string;
}
