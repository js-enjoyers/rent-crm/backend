import { Transform } from 'class-transformer';

import {
  IsBoolean,
  IsNotEmpty,
  IsNumber,
  IsNumberString,
  IsOptional,
  IsPositive,
  Length,
} from 'class-validator';

import {
  MIN_GUEST_PHONE_LENGTH,
  MAX_GUEST_PHONE_LENGTH,
} from '../../../declarations';

import { deepTrim } from '../../../utils';

export class CreatePhoneDto {
  @Transform((params) => deepTrim(params.value))
  @Length(MIN_GUEST_PHONE_LENGTH, MAX_GUEST_PHONE_LENGTH)
  @IsNotEmpty()
  @IsNumberString()
  public value: string;

  @IsNotEmpty()
  @IsNumber()
  @IsPositive()
  public guestId: number;

  @IsBoolean()
  @IsOptional()
  public force?: boolean;
}
