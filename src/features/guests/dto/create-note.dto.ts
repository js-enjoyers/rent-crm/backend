import { Transform } from 'class-transformer';

import {
  IsNotEmpty,
  IsNumber,
  IsPositive,
  IsString,
  Length,
} from 'class-validator';

import {
  MIN_GUEST_NOTE_LENGTH,
  MAX_GUEST_NOTE_LENGTH,
} from '../../../declarations';

export class CreateNoteDto {
  @Transform((params) => params.value.trim())
  @Length(MIN_GUEST_NOTE_LENGTH, MAX_GUEST_NOTE_LENGTH)
  @IsNotEmpty()
  @IsString()
  public text: string;

  @IsNotEmpty()
  @IsNumber()
  @IsPositive()
  public guestId: number;
}
