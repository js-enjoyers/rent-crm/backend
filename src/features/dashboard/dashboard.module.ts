import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { PropertiesModule } from '../properties/properties.module';

import { DashboardController } from './dashboard.controller';
import { DashboardService } from './dashboard.service';
import { MonthlyStatsService } from './stats/monthly-stats.service';
import { YearlyStatsService } from './stats/yearly-stats.service';

import { Booking } from '../../entities/booking.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Booking]), PropertiesModule],
  controllers: [DashboardController],
  providers: [DashboardService, MonthlyStatsService, YearlyStatsService],
})
export class DashboardModule {}
