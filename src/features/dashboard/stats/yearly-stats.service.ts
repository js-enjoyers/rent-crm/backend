import { DateTime } from 'luxon';

import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { Booking } from '../../../entities/booking.entity';
import { Property } from '../../../entities/property.entity';

import { PropertiesService } from '../../properties/properties.service';
import { BookingStatus } from '../../../declarations';

type GetStatsOptions = {
  from: Date;
  to: Date;
  property?: Property;
};

@Injectable()
export class YearlyStatsService {
  constructor(
    @InjectRepository(Booking)
    private readonly bookingsRepository: Repository<Booking>,
    private readonly propertiesService: PropertiesService,
  ) {}

  public async calculate(options: GetStatsOptions) {
    const [
      { totalProfit, recordProfit, totalGuests, recordGuests, bookingsCount },
      { profitGraphData, guestsGraphData, bookingsGraphData },
      favoriteProperty,
    ] = await Promise.all([
      this.getNumericStats(options),
      this.getGraphsData(options),
      this.getFavoriteProperty(options),
    ]);

    return {
      totalProfit,
      recordProfit,
      totalGuests,
      recordGuests,
      bookingsCount,
      favoriteProperty,
      profitGraphData,
      guestsGraphData,
      bookingsGraphData,
    };
  }

  private async getNumericStats({ from, to, property }: GetStatsOptions) {
    const query = this.bookingsRepository
      .createQueryBuilder('booking')
      .select('SUM(booking.totalProfit)', 'totalProfit')
      .addSelect('MAX(booking.totalProfit)', 'recordProfit')
      .addSelect('SUM(booking.adults + booking.children)', 'totalGuests')
      .addSelect('MAX(booking.adults + booking.children)', 'recordGuests')
      .addSelect('COUNT(booking.id)', 'bookingsCount');

    query.andWhere('booking.startDate BETWEEN :from AND :to', {
      from: from.toUTCString(),
      to: to.toUTCString(),
    });

    query.andWhere('booking.status != :status', {
      status: BookingStatus.CANCELLED,
    });

    if (property) {
      query.andWhere('booking.propertyId = :propertyId', {
        propertyId: property.id,
      });
    }

    const stats = await query.getRawOne();

    return {
      totalProfit: stats?.totalProfit
        ? this.formatProfit(stats.totalProfit)
        : 0,
      recordProfit: stats?.recordProfit
        ? this.formatProfit(stats.recordProfit)
        : 0,
      totalGuests: stats?.totalGuests
        ? this.formatNumber(stats.totalGuests)
        : 0,
      recordGuests: stats?.recordGuests
        ? this.formatNumber(stats.recordGuests)
        : 0,
      bookingsCount: stats?.bookingsCount
        ? this.formatNumber(stats.bookingsCount)
        : 0,
    };
  }

  private async getFavoriteProperty({ from, to, property }: GetStatsOptions) {
    const query = this.bookingsRepository
      .createQueryBuilder('booking')
      .select('booking.propertyId', 'propertyId')
      .addSelect('COUNT(booking.id)', 'bookingCount')
      .groupBy('"propertyId"')
      .orderBy('"bookingCount"', 'DESC')
      .limit(1);

    query.andWhere('booking.startDate BETWEEN :from AND :to', {
      from: from.toUTCString(),
      to: to.toUTCString(),
    });

    query.andWhere('booking.status != :status', {
      status: BookingStatus.CANCELLED,
    });

    if (property) {
      query.andWhere('booking.propertyId = :propertyId', {
        propertyId: property.id,
      });
    }

    const stats = await query.getRawOne();
    if (!stats?.propertyId) return { name: 'Нет данных' };

    return this.propertiesService
      .getOne({
        id: stats.propertyId,
      })
      .then((targetProperty) => {
        return { name: targetProperty!.name };
      });
  }

  private async getGraphsData({ from, to, property }: GetStatsOptions) {
    const query = this.bookingsRepository.createQueryBuilder('booking');

    query.andWhere('booking.startDate BETWEEN :from AND :to', {
      from: from.toUTCString(),
      to: to.toUTCString(),
    });

    query.andWhere('booking.status != :status', {
      status: BookingStatus.CANCELLED,
    });

    if (property) {
      query.andWhere('booking.propertyId = :propertyId', {
        propertyId: property.id,
      });
    }

    query.orderBy('booking.startDate', 'ASC');

    const bookings = await query.getMany();
    const monthsMap = this.getMonthsMap(from, to);

    bookings.forEach((booking) => {
      const month = DateTime.fromJSDate(new Date(booking.startDate)).month;

      const existingValue = monthsMap.get(month) ?? {
        profit: 0,
        guests: 0,
        bookings: 0,
      };

      const profit = existingValue.profit + Number(booking.totalProfit);
      const guests = existingValue.guests + booking.adults + booking.children;
      const bookings = existingValue.bookings + 1;

      monthsMap.set(month, { profit, guests, bookings });
    });

    const graphsData = Array.from(monthsMap.values());

    const profitGraphData = graphsData.map(({ profit }) => profit);
    const guestsGraphData = graphsData.map(({ guests }) => guests);
    const bookingsGraphData = graphsData.map(({ bookings }) => bookings);

    return { profitGraphData, guestsGraphData, bookingsGraphData };
  }

  private getMonthsMap(from: Date, to: Date) {
    const monthsMap: Map<
      number,
      { profit: number; guests: number; bookings: number }
    > = new Map();

    const start = DateTime.fromJSDate(from, { zone: 'UTC' });
    const end = DateTime.fromJSDate(to, { zone: 'UTC' });

    for (let i = start.month; i <= end.month; i++) {
      monthsMap.set(i, { profit: 0, guests: 0, bookings: 0 });
    }

    return monthsMap;
  }

  private formatProfit(profit: string | number) {
    return parseFloat(Number(profit).toFixed(2));
  }

  private formatNumber(value: string | number) {
    return Math.floor(Number(value));
  }
}
