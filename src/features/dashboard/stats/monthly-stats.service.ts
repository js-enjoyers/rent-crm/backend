import { DateTime } from 'luxon';

import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { Booking } from '../../../entities/booking.entity';
import { Property } from '../../../entities/property.entity';

import { PropertiesService } from '../../properties/properties.service';
import { BookingStatus } from '../../../declarations';

type GetStatsOptions = {
  from: Date;
  to: Date;
  property?: Property;
};

@Injectable()
export class MonthlyStatsService {
  constructor(
    @InjectRepository(Booking)
    private readonly bookingsRepository: Repository<Booking>,
    private readonly propertiesService: PropertiesService,
  ) {}

  public async calculate(options: GetStatsOptions) {
    const [
      { totalProfit, averageProfit, totalGuests, averageGuests, bookingsCount },
      { profitGraphData, guestsGraphData },
      favoriteProperty,
    ] = await Promise.all([
      this.getNumericStats(options),
      this.getGraphsData(options),
      this.getFavoriteProperty(options),
    ]);

    return {
      totalProfit,
      averageProfit,
      totalGuests,
      averageGuests,
      bookingsCount,
      favoriteProperty,
      profitGraphData,
      guestsGraphData,
    };
  }

  private async getNumericStats({ from, to, property }: GetStatsOptions) {
    const query = this.bookingsRepository
      .createQueryBuilder('booking')
      .select('SUM(booking.totalProfit)', 'totalProfit')
      .addSelect('AVG(booking.totalProfit)', 'averageProfit')
      .addSelect('SUM(booking.adults + booking.children)', 'totalGuests')
      .addSelect('AVG(booking.adults + booking.children)', 'averageGuests')
      .addSelect('COUNT(booking.id)', 'bookingsCount');

    query.andWhere('booking.startDate BETWEEN :from AND :to', {
      from: from.toUTCString(),
      to: to.toUTCString(),
    });

    query.andWhere('booking.status != :status', {
      status: BookingStatus.CANCELLED,
    });

    if (property) {
      query.andWhere('booking.propertyId = :propertyId', {
        propertyId: property.id,
      });
    }

    const stats = await query.getRawOne();

    return {
      totalProfit: stats?.totalProfit
        ? this.formatProfit(stats.totalProfit)
        : 0,
      averageProfit: stats?.averageProfit
        ? this.formatProfit(stats.averageProfit)
        : 0,
      totalGuests: stats?.totalGuests
        ? this.formatNumber(stats.totalGuests)
        : 0,
      averageGuests: stats?.averageGuests
        ? this.formatNumber(stats.averageGuests)
        : 0,
      bookingsCount: stats?.bookingsCount
        ? this.formatNumber(stats.bookingsCount)
        : 0,
    };
  }

  private async getFavoriteProperty({ from, to, property }: GetStatsOptions) {
    const query = this.bookingsRepository
      .createQueryBuilder('booking')
      .select('booking.propertyId', 'propertyId')
      .addSelect('COUNT(booking.id)', 'bookingCount')
      .groupBy('"propertyId"')
      .orderBy('"bookingCount"', 'DESC')
      .limit(1);

    query.andWhere('booking.startDate BETWEEN :from AND :to', {
      from: from.toUTCString(),
      to: to.toUTCString(),
    });

    query.andWhere('booking.status != :status', {
      status: BookingStatus.CANCELLED,
    });

    if (property) {
      query.andWhere('booking.propertyId = :propertyId', {
        propertyId: property.id,
      });
    }

    const stats = await query.getRawOne();
    if (!stats?.propertyId) return { name: 'Нет данных' };

    return this.propertiesService
      .getOne({
        id: stats.propertyId,
      })
      .then((targetProperty) => {
        return { name: targetProperty!.name };
      });
  }

  private async getGraphsData({ from, to, property }: GetStatsOptions) {
    const query = this.bookingsRepository.createQueryBuilder('booking');

    query.andWhere('booking.startDate BETWEEN :from AND :to', {
      from: from.toUTCString(),
      to: to.toUTCString(),
    });

    query.andWhere('booking.status != :status', {
      status: BookingStatus.CANCELLED,
    });

    if (property) {
      query.andWhere('booking.propertyId = :propertyId', {
        propertyId: property.id,
      });
    }

    query.orderBy('booking.startDate', 'ASC');

    const bookings = await query.getMany();
    const datesMap = this.getDatesMap(from, to);

    bookings.forEach((booking) => {
      const date = DateTime.fromJSDate(
        new Date(booking.startDate),
      ).toISODate()!;

      const existingValue = datesMap.get(date) ?? {
        profit: 0,
        guests: 0,
      };

      const profit = existingValue.profit + Number(booking.totalProfit);
      const guests = existingValue.guests + booking.adults + booking.children;

      datesMap.set(date, { profit, guests });
    });

    const graphsData = Array.from(datesMap.values()).reduce<
      { profit: number; guests: number }[]
    >((acc, { profit, guests }) => {
      const previousValue =
        acc.length > 0 ? acc[acc.length - 1] : { profit: 0, guests: 0 };

      const nextProfit = previousValue.profit + profit;
      const nextGuests = previousValue.guests + guests;

      acc.push({ profit: nextProfit, guests: nextGuests });
      return acc;
    }, []);

    const profitGraphData = graphsData.map(({ profit }) => profit);
    const guestsGraphData = graphsData.map(({ guests }) => guests);

    return { profitGraphData, guestsGraphData };
  }

  private getDatesMap(from: Date, to: Date) {
    const dateMap: Map<string, { profit: number; guests: number }> = new Map();

    const start = DateTime.fromJSDate(from, { zone: 'UTC' });
    const end = DateTime.fromJSDate(to, { zone: 'UTC' });

    let currentDate = start;
    while (currentDate <= end) {
      dateMap.set(currentDate.toISODate()!, { profit: 0, guests: 0 });
      currentDate = currentDate.plus({ days: 1 });
    }

    return dateMap;
  }

  private formatProfit(profit: string | number) {
    return parseFloat(Number(profit).toFixed(2));
  }

  private formatNumber(value: string | number) {
    return Math.floor(Number(value));
  }
}
