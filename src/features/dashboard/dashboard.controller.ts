import {
  BadRequestException,
  Controller,
  DefaultValuePipe,
  Get,
  ParseIntPipe,
  Query,
} from '@nestjs/common';

import { DashboardService } from './dashboard.service';
import { PropertiesService } from '../properties/properties.service';

import { MonthlyStatsService } from './stats/monthly-stats.service';
import { YearlyStatsService } from './stats/yearly-stats.service';

import { ParseDatePipe } from '../../common/pipes';

@Controller('dashboard')
export class DashboardController {
  constructor(
    private readonly propertiesService: PropertiesService,
    private readonly dashboardService: DashboardService,
    private readonly monthlyStatsService: MonthlyStatsService,
    private readonly yearlyStatsService: YearlyStatsService,
  ) {}

  @Get('boundaries')
  public getBoundaries() {
    return this.dashboardService.getBoundaries();
  }

  @Get('stats/monthly')
  public async getMonthlyStats(
    @Query('start', ParseDatePipe) start: Date,
    @Query('end', ParseDatePipe) end: Date,
    @Query('propertyId', new DefaultValuePipe(0), ParseIntPipe)
      propertyId: number, // eslint-disable-line -- Fixes prettier conflict
  ) {
    if (!propertyId) {
      return this.monthlyStatsService.calculate({ from: start, to: end });
    }

    const property = await this.propertiesService.getOne({ id: propertyId });

    if (!property) {
      throw new BadRequestException('Объект не найден');
    }

    return this.monthlyStatsService.calculate({
      from: start,
      to: end,
      property,
    });
  }

  @Get('stats/yearly')
  public async getYearlyStats(
    @Query('start', ParseDatePipe) start: Date,
    @Query('end', ParseDatePipe) end: Date,
    @Query('propertyId', new DefaultValuePipe(0), ParseIntPipe)
      propertyId: number, // eslint-disable-line -- Fixes prettier conflict
  ) {
    if (!propertyId) {
      return this.yearlyStatsService.calculate({ from: start, to: end });
    }

    const property = await this.propertiesService.getOne({ id: propertyId });

    if (!property) {
      throw new BadRequestException('Объект не найден');
    }

    return this.yearlyStatsService.calculate({
      from: start,
      to: end,
      property,
    });
  }
}
