import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { Booking } from '../../entities/booking.entity';

@Injectable()
export class DashboardService {
  constructor(
    @InjectRepository(Booking)
    private readonly bookingsRepository: Repository<Booking>,
  ) {}

  public async getBoundaries() {
    const [[firstBooking], [lastBooking]] = await Promise.all([
      this.bookingsRepository.find({ take: 1, order: { startDate: 'ASC' } }),
      this.bookingsRepository.find({ take: 1, order: { endDate: 'DESC' } }),
    ]);

    return {
      start: firstBooking?.startDate ?? new Date().toUTCString(),
      end: lastBooking?.endDate ?? new Date().toUTCString(),
    };
  }
}
