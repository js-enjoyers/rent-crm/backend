import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindManyOptions, ILike, Repository } from 'typeorm';

import { Property } from '../../entities/property.entity';

@Injectable()
export class PropertiesService {
  constructor(
    @InjectRepository(Property)
    private readonly propertiesRepository: Repository<Property>,
  ) {}

  public get({ term, limit, offset, withArchived }: DefaultQueryOptions) {
    const findOptions: FindManyOptions<Property> = {
      order: { createdAt: 'DESC' },
      withDeleted: withArchived,
      take: limit,
      skip: offset,
    };

    if (term) {
      findOptions.where = { name: ILike(`%${term}%`) };
    }

    return this.propertiesRepository.find(findOptions);
  }

  public getOne({ id, name }: Partial<Pick<Property, 'id' | 'name'>>) {
    const whereOptions = [];

    if (id) whereOptions.push({ id });
    if (name) whereOptions.push({ name: ILike(name) });

    return this.propertiesRepository.findOne({
      where: whereOptions,
      withDeleted: true,
    });
  }

  public async create(payload: Pick<Property, 'name' | 'capacity' | 'color'>) {
    const {
      identifiers: [{ id }],
    } = await this.propertiesRepository.insert(payload);

    return this.getOne({ id });
  }

  public async update(
    id: Property['id'],
    payload: Pick<Property, 'name' | 'capacity' | 'color'>,
  ) {
    await this.propertiesRepository.update({ id }, payload);
    return this.getOne({ id });
  }

  public async archive(id: Property['id']) {
    await this.propertiesRepository.softDelete({ id });
    return this.getOne({ id });
  }

  public async restore(id: Property['id']) {
    await this.propertiesRepository.restore({ id });
    return this.getOne({ id });
  }
}
