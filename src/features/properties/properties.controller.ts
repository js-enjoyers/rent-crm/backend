import {
  BadRequestException,
  Body,
  Controller,
  DefaultValuePipe,
  Delete,
  Get,
  NotFoundException,
  Param,
  ParseBoolPipe,
  ParseIntPipe,
  Post,
  Put,
  Query,
} from '@nestjs/common';

import { PropertiesService } from './properties.service';

import { CreatePropertyDto } from './dto/create-property.dto';
import { UpdatePropertyDto } from './dto/update-property.dto';

import { TrimPipe } from '../../common/pipes';

@Controller('properties')
export class PropertiesController {
  constructor(private readonly propertiesService: PropertiesService) {}

  @Get()
  public get(
    @Query('term', new TrimPipe()) term?: string,
    @Query('limit', new DefaultValuePipe(20), ParseIntPipe) limit?: number,
    @Query('offset', new DefaultValuePipe(0), ParseIntPipe) offset?: number,
    @Query('archive', new DefaultValuePipe(false), ParseBoolPipe)
      withArchived?: boolean, // eslint-disable-line -- Fixes prettier conflict
  ) {
    return this.propertiesService.get({ term, limit, offset, withArchived });
  }

  @Post()
  public async create(@Body() payload: CreatePropertyDto) {
    const property = await this.propertiesService.getOne({
      name: payload.name,
    });

    if (property) {
      throw new BadRequestException('Объект с таким именем уже существует');
    }

    return this.propertiesService.create(payload);
  }

  @Put(':id')
  public async update(
    @Param('id', ParseIntPipe) id: number,
    @Body() payload: UpdatePropertyDto,
  ) {
    const targetProperty = await this.propertiesService.getOne({ id });

    if (!targetProperty) {
      throw new NotFoundException('Объект не найден');
    }

    if (targetProperty.isArchived()) {
      throw new BadRequestException('Объект в архиве');
    }

    const propertyByName = await this.propertiesService.getOne({
      name: payload.name,
    });

    if (propertyByName && propertyByName.id !== id) {
      throw new BadRequestException('Объект с таким именем уже существует');
    }

    return this.propertiesService.update(id, payload);
  }

  @Delete(':id')
  public async archive(@Param('id', ParseIntPipe) id: number) {
    const property = await this.propertiesService.getOne({ id });

    if (!property) {
      throw new NotFoundException('Объект не найден');
    }

    return this.propertiesService.archive(id);
  }

  @Put(':id/restore')
  public async restore(@Param('id', ParseIntPipe) id: number) {
    const property = await this.propertiesService.getOne({ id });

    if (!property) {
      throw new NotFoundException('Объект не найден');
    }

    return this.propertiesService.restore(id);
  }
}
