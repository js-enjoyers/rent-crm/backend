import { CreatePropertyDto } from './create-property.dto';

export class UpdatePropertyDto extends CreatePropertyDto {}
