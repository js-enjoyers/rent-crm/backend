import { Transform } from 'class-transformer';

import {
  IsInt,
  IsNotEmpty,
  IsString,
  Length,
  Min,
  Validate,
} from 'class-validator';

import {
  MIN_PROPERTY_NAME_LENGTH,
  MAX_PROPERTY_NAME_LENGTH,
  MIN_PROPERTY_CAPACITY,
} from '../../../declarations';

import { IsColor } from '../../../common/validators/color.validator';
import { deepTrim } from '../../../utils';

export class CreatePropertyDto {
  @Transform((params) => params.value.trim())
  @Length(MIN_PROPERTY_NAME_LENGTH, MAX_PROPERTY_NAME_LENGTH)
  @IsNotEmpty()
  @IsString()
  public name: string;

  @IsInt()
  @Min(MIN_PROPERTY_CAPACITY)
  public capacity: number;

  @Transform((params) => {
    return deepTrim(params.value).toUpperCase();
  })
  @Validate(IsColor)
  public color: string;
}
