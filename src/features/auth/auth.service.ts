import { Injectable } from '@nestjs/common';

import { JwtService } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';

import { User } from '../../entities/user.entity';
import { applyOmit } from '../../utils';

@Injectable()
export class AuthService {
  constructor(
    private readonly configService: ConfigService,
    private readonly jwtService: JwtService,
  ) {}

  public async login(user: User) {
    const secret = this.configService.get<string>('JWT_SECRET');
    const jwtTtl = this.configService.get<string>('JWT_TTL');

    const jwtEncodePayload = applyOmit(user, ['pin']);
    const jwtString = await this.jwtService.signAsync(jwtEncodePayload, {
      expiresIn: jwtTtl,
      secret,
    });

    return {
      accessToken: jwtString,
      ...jwtEncodePayload,
    };
  }
}
