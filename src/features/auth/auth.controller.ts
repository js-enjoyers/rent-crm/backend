import { compare } from 'bcrypt';

import {
  BadRequestException,
  ForbiddenException,
  Body,
  Controller,
  Get,
  Post,
  Param,
} from '@nestjs/common';

import { Public } from '../../common/decorators/public.decorator';
import { CredentialsDto } from '../../common/dto/credentials.dto';

import { UsersService } from '../users/users.service';
import { AuthService } from './auth.service';

import { TrimPipe } from '../../common/pipes';
import { applyOmit } from '../../utils';

@Controller('auth')
export class AuthController {
  constructor(
    private readonly usersService: UsersService,
    private readonly authService: AuthService,
  ) {}

  @Public()
  @Get('setup')
  public async checkAppInitialized() {
    const usersTotal = await this.usersService.getTotalCount();
    return { isAppInitialized: usersTotal > 0 };
  }

  @Public()
  @Post('setup')
  public async setupFirstUser(@Body() payload: CredentialsDto) {
    const usersTotal = await this.usersService.getTotalCount();

    if (usersTotal > 0) {
      throw new BadRequestException('Приложение уже готово к работе');
    }

    const createdUser = await this.usersService.create(payload);
    return this.authService.login(createdUser!);
  }

  @Public()
  @Post('login')
  public async login(@Body() payload: CredentialsDto) {
    const user = await this.usersService.getOne({
      login: payload.login,
    });

    const isPinValid = user ? await compare(payload.pin, user.pin) : false;

    if (!user || !isPinValid) {
      throw new ForbiddenException('Неверные учетные данные');
    }

    return this.authService.login(user);
  }

  @Public()
  @Get('login/:targetLogin')
  public async checkAccount(
    @Param('targetLogin', new TrimPipe({ deep: true })) login: string,
  ) {
    const user = await this.usersService.getOne(
      { login },
      { caseSensitive: true },
    );

    if (!user) {
      throw new BadRequestException('Аккаунт с таким логином не существует');
    }

    return applyOmit(user, ['pin']);
  }
}
