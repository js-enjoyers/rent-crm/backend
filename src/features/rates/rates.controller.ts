import {
  BadRequestException,
  Body,
  Controller,
  DefaultValuePipe,
  Delete,
  Get,
  NotFoundException,
  Param,
  ParseBoolPipe,
  ParseIntPipe,
  Post,
  Put,
  Query,
} from '@nestjs/common';

import { RatesService } from './rates.service';

import { CreateRateDto } from './dto/create-rate.dto';
import { UpdateRateDto } from './dto/update-rate.dto';

import { TrimPipe } from '../../common/pipes';

@Controller('rates')
export class RatesController {
  constructor(private readonly ratesService: RatesService) {}

  @Get()
  public async get(
    @Query('term', new TrimPipe()) term?: string,
    @Query('limit', new DefaultValuePipe(20), ParseIntPipe) limit?: number,
    @Query('offset', new DefaultValuePipe(0), ParseIntPipe) offset?: number,
    @Query('archive', new DefaultValuePipe(false), ParseBoolPipe)
      withArchived?: boolean, // eslint-disable-line -- Fixes prettier conflict
  ) {
    return this.ratesService.get({ term, limit, offset, withArchived });
  }

  @Post()
  public async create(@Body() payload: CreateRateDto) {
    const rate = await this.ratesService.getOne({
      name: payload.name,
    });

    if (rate) {
      throw new BadRequestException('Тариф с таким именем уже существует');
    }

    return this.ratesService.create(payload);
  }

  @Put(':id')
  public async update(
    @Param('id', ParseIntPipe) id: number,
    @Body() payload: UpdateRateDto,
  ) {
    const targetRate = await this.ratesService.getOne({ id });

    if (!targetRate) {
      throw new NotFoundException('Тариф не найден');
    }

    if (targetRate.isArchived()) {
      throw new BadRequestException('Тариф в архиве');
    }

    const rateByName = await this.ratesService.getOne({
      name: payload.name,
    });

    if (rateByName && rateByName.id !== id) {
      throw new BadRequestException('Тариф с таким именем уже существует');
    }

    return this.ratesService.update(id, payload);
  }

  @Delete(':id')
  public async archive(@Param('id', ParseIntPipe) id: number) {
    const rate = await this.ratesService.getOne({ id });

    if (!rate) {
      throw new NotFoundException('Тариф не найден');
    }

    return this.ratesService.archive(id);
  }

  @Put(':id/restore')
  public async restore(@Param('id', ParseIntPipe) id: number) {
    const rate = await this.ratesService.getOne({ id });

    if (!rate) {
      throw new NotFoundException('Тариф не найден');
    }

    return this.ratesService.restore(id);
  }
}
