import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindManyOptions, ILike, Repository } from 'typeorm';

import { Rate } from '../../entities/rate.entity';

@Injectable()
export class RatesService {
  constructor(
    @InjectRepository(Rate)
    private readonly ratesRepository: Repository<Rate>,
  ) {}

  public get({ term, limit, offset, withArchived }: DefaultQueryOptions) {
    const findOptions: FindManyOptions<Rate> = {
      order: { createdAt: 'DESC' },
      withDeleted: withArchived,
      take: limit,
      skip: offset,
    };

    if (term) {
      findOptions.where = { name: ILike(`%${term}%`) };
    }

    return this.ratesRepository.find(findOptions);
  }

  public getOne({ id, name }: Partial<Pick<Rate, 'id' | 'name'>>) {
    const whereOptions = [];

    if (id) whereOptions.push({ id });
    if (name) whereOptions.push({ name: ILike(name) });

    return this.ratesRepository.findOne({
      where: whereOptions,
      withDeleted: true,
    });
  }

  public async create(payload: Pick<Rate, 'name' | 'profit'>) {
    const {
      identifiers: [{ id }],
    } = await this.ratesRepository.insert(payload);

    return this.getOne({ id });
  }

  public async update(id: Rate['id'], payload: Pick<Rate, 'name' | 'profit'>) {
    await this.ratesRepository.update({ id }, payload);
    return this.getOne({ id });
  }

  public async archive(id: Rate['id']) {
    await this.ratesRepository.softDelete({ id });
    return this.getOne({ id });
  }

  public async restore(id: Rate['id']) {
    await this.ratesRepository.restore({ id });
    return this.getOne({ id });
  }
}
