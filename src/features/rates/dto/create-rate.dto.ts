import { Transform } from 'class-transformer';
import { IsNotEmpty, IsString, Length, Validate } from 'class-validator';

import {
  MIN_RATE_NAME_LENGTH,
  MAX_RATE_NAME_LENGTH,
} from '../../../declarations';

import { IsProfit } from '../../../common/validators/profit.validator';
import { parseProfit } from '../../../utils';

export class CreateRateDto {
  @Transform((params) => params.value.trim())
  @Length(MIN_RATE_NAME_LENGTH, MAX_RATE_NAME_LENGTH)
  @IsNotEmpty()
  @IsString()
  public name: string;

  @Transform((params) => parseProfit(params.value))
  @Validate(IsProfit)
  public profit: number;
}
