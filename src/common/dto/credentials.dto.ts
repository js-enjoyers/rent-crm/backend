import { Transform } from 'class-transformer';
import { IsNotEmpty, IsString, Length } from 'class-validator';

import {
  MIN_USER_LOGIN_LENGTH,
  MAX_USER_LOGIN_LENGTH,
  USER_PIN_LENGTH,
} from '../../declarations';

import { deepTrim } from '../../utils';

/**
 * Used to create user or authorization since
 * user model itself is very simple and has
 * very few fields
 */
export class CredentialsDto {
  @Transform((params) => deepTrim(params.value))
  @Length(MIN_USER_LOGIN_LENGTH, MAX_USER_LOGIN_LENGTH)
  @IsNotEmpty()
  @IsString()
  public login: string;

  @Transform((params) => deepTrim(params.value))
  @Length(USER_PIN_LENGTH, USER_PIN_LENGTH)
  @IsNotEmpty()
  @IsString()
  public pin: string;
}
