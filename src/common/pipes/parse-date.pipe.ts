import { DateTime } from 'luxon';
import { PipeTransform, Injectable, BadRequestException } from '@nestjs/common';

import { isDefined } from '../../utils';

@Injectable()
export class ParseDatePipe implements PipeTransform {
  public transform(value: unknown) {
    if (!isDefined(value)) return value;

    if (typeof value !== 'string') {
      throw new BadRequestException(
        'Значение должно быть строкой в формате ISO8601',
      );
    }

    const date = DateTime.fromISO(decodeURIComponent(value));

    if (!date.isValid) {
      throw new BadRequestException(
        'Значение должно быть строкой в формате ISO8601',
      );
    }

    return date.toUTC().toJSDate();
  }
}
