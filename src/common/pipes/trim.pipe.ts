import { PipeTransform, Injectable } from '@nestjs/common';

type TrimPipeOptions = {
  deep: boolean;
};

@Injectable()
export class TrimPipe implements PipeTransform {
  constructor(private options: TrimPipeOptions = { deep: false }) {}

  public transform(value: unknown) {
    if (typeof value !== 'string') {
      return value;
    }

    return this.options.deep ? value.replace(' ', '') : value.trim();
  }
}
