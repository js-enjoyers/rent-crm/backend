import { DataSource, QueryRunner } from 'typeorm';
import { Injectable } from '@nestjs/common';

class Transaction {
  constructor(private readonly queryRunner: QueryRunner) {}

  public get manager() {
    return this.queryRunner.manager;
  }

  public async commit() {
    await this.queryRunner.commitTransaction();
    await this.queryRunner.release();
  }

  public async rollback() {
    await this.queryRunner.rollbackTransaction();
    await this.queryRunner.release();
  }
}

@Injectable()
class TransactionFactory {
  constructor(private readonly dataSource: DataSource) {}

  public async create() {
    const queryRunner = this.dataSource.createQueryRunner();

    await queryRunner.connect();
    await queryRunner.startTransaction();

    return new Transaction(queryRunner);
  }
}

export { TransactionFactory, Transaction };
