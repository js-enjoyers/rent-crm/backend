import {
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';

@ValidatorConstraint({ name: 'is-color', async: false })
export class IsColor implements ValidatorConstraintInterface {
  public validate(value: unknown) {
    if (typeof value !== 'string') {
      return false;
    }

    // HEX color format: #RRGGBB
    const hexColorPattern = /^#[0-9A-F]{6}$/i;
    return hexColorPattern.test(value);
  }

  public defaultMessage() {
    return 'Формат цвета не валиден';
  }
}
