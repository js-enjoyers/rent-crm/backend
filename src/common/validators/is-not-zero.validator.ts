import {
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';

@ValidatorConstraint({ name: 'is-not-zero', async: false })
export class IsNotZero implements ValidatorConstraintInterface {
  public validate(value: unknown) {
    const parsedValue = parseFloat(String(value));

    if (isNaN(parsedValue)) return false;
    return parsedValue !== 0;
  }

  public defaultMessage() {
    return 'Значение не должно быть нулевым';
  }
}
