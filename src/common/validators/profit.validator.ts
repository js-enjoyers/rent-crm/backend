import {
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';

@ValidatorConstraint({ name: 'is-profit', async: false })
export class IsProfit implements ValidatorConstraintInterface {
  public validate(value: unknown) {
    const parsedValue = parseFloat(String(value));

    if (isNaN(parsedValue)) return false;
    return parsedValue >= 0;
  }

  public defaultMessage() {
    return 'Значение должно быть положительным числом';
  }
}
