import { Column, DeleteDateColumn, Entity, OneToMany } from 'typeorm';

import { BaseEntity } from './base.entity';
import { Booking } from './booking.entity';

import { MAX_PROPERTY_NAME_LENGTH } from '../declarations';

@Entity({ name: 'properties' })
export class Property extends BaseEntity {
  @Column({
    type: 'varchar',
    length: MAX_PROPERTY_NAME_LENGTH,
    nullable: false,
  })
  public name: string;

  @Column({ type: 'smallint', nullable: false })
  public capacity: number;

  // HEX color of property
  @Column({ type: 'varchar', length: 8, nullable: false })
  public color: string;

  @DeleteDateColumn()
  public deletedAt: Date | null;

  @OneToMany(() => Booking, (booking) => booking.property)
  public bookings: Booking[];
}
