import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

import { MAX_BOOKING_CORRECTION_REASON_LENGTH } from '../declarations';
import { Booking } from './booking.entity';

@Entity({ name: 'corrections' })
export class Correction {
  // UUID because there are no limit of corrections count in booking
  @PrimaryGeneratedColumn('uuid')
  public id: string;

  @Column({
    nullable: false,
    type: 'decimal',
    precision: 10,
    scale: 2,
  })
  public value: string | number;

  @Column({
    type: 'varchar',
    length: MAX_BOOKING_CORRECTION_REASON_LENGTH,
    nullable: false,
  })
  public reason: string;

  @Column({ type: 'int' })
  public bookingId: number;

  @ManyToOne(() => Booking, (booking) => booking.corrections, {
    nullable: false,
  })
  @JoinColumn({ name: 'bookingId' })
  public booking: Booking;
}
