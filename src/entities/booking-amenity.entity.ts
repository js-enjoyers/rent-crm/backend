import { Min } from 'class-validator';

import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

import { Booking } from './booking.entity';
import { Amenity } from './amenity.entity';

@Entity({ name: 'bookings_amenities' })
export class BookingAmenity {
  @PrimaryGeneratedColumn('uuid')
  public id: string;

  @Column({
    nullable: false,
    type: 'decimal',
    precision: 10,
    scale: 2,
    comment: 'Static value of amenity profit to prevent it from changes',
  })
  public appliedProfit: string | number;

  @Column({ type: 'int', nullable: false })
  @Min(1)
  public quantity: number;

  @Column({ type: 'int' })
  public bookingId: number;

  @Column({ type: 'int' })
  public amenityId: number;

  @ManyToOne(() => Booking, (booking) => booking.bookingsAmenities)
  @JoinColumn({ name: 'bookingId' })
  public booking: Booking;

  @ManyToOne(() => Amenity, (amenity) => amenity.bookingsAmenities)
  @JoinColumn({ name: 'amenityId' })
  public amenity: Amenity;
}
