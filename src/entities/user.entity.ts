import { Column, Entity } from 'typeorm';

import { BaseEntity } from './base.entity';
import { MAX_USER_LOGIN_LENGTH } from '../declarations';

@Entity({ name: 'users' })
export class User extends BaseEntity {
  @Column({
    type: 'varchar',
    length: MAX_USER_LOGIN_LENGTH,
    unique: true,
    nullable: false,
  })
  public login: string;

  @Column({ nullable: false })
  public pin: string;
}
