import { Column, Entity, OneToMany } from 'typeorm';

import { BaseEntity } from './base.entity';
import { Phone } from './phone.entity';
import { Note } from './note.entity';
import { Booking } from './booking.entity';

import { MAX_GUEST_NAME_LENGTH } from '../declarations';

@Entity({ name: 'guests' })
export class Guest extends BaseEntity {
  @Column({
    type: 'varchar',
    length: MAX_GUEST_NAME_LENGTH,
    nullable: false,
  })
  public name: string;

  @OneToMany(() => Phone, (phone) => phone.guest)
  public phones: Phone[];

  @OneToMany(() => Note, (note) => note.guest)
  public notes: Note[];

  @OneToMany(() => Booking, (booking) => booking.guest)
  public bookings: Booking[];
}
