import { Column, DeleteDateColumn, Entity, ManyToMany } from 'typeorm';

import { BaseEntity } from './base.entity';
import { BookingAmenity } from './booking-amenity.entity';

import { MAX_AMENITY_NAME_LENGTH } from '../declarations';

@Entity({ name: 'amenities' })
export class Amenity extends BaseEntity {
  @Column({
    type: 'varchar',
    length: MAX_AMENITY_NAME_LENGTH,
    nullable: false,
  })
  public name: string;

  @Column({
    nullable: false,
    type: 'decimal',
    precision: 10,
    scale: 2,
  })
  public profit: string | number;

  @DeleteDateColumn()
  public deletedAt: Date | null;

  @ManyToMany(() => BookingAmenity, (bookingAmenity) => bookingAmenity.amenity)
  public bookingsAmenities: BookingAmenity[];
}
