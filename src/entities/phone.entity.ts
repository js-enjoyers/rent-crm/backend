import { Column, Entity, JoinColumn, ManyToOne } from 'typeorm';

import { BaseEntity } from './base.entity';
import { Guest } from './guest.entity';

import { MAX_GUEST_PHONE_LENGTH } from '../declarations';

@Entity({ name: 'phones' })
export class Phone extends BaseEntity {
  @Column({
    type: 'varchar',
    length: MAX_GUEST_PHONE_LENGTH,
    unique: true,
    nullable: false,
  })
  public value: string;

  @Column({ type: 'int' })
  public guestId: number;

  @ManyToOne(() => Guest, (guest) => guest.phones, { nullable: false })
  @JoinColumn({ name: 'guestId' })
  public guest: Guest;
}
