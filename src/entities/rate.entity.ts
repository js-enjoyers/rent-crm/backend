import { Column, DeleteDateColumn, Entity, OneToMany } from 'typeorm';

import { BaseEntity } from './base.entity';
import { Booking } from './booking.entity';

import { MAX_RATE_NAME_LENGTH } from '../declarations';

@Entity({ name: 'rates' })
export class Rate extends BaseEntity {
  @Column({
    type: 'varchar',
    length: MAX_RATE_NAME_LENGTH,
    nullable: false,
  })
  public name: string;

  @Column({
    nullable: false,
    type: 'decimal',
    precision: 10,
    scale: 2,
  })
  public profit: string | number;

  @DeleteDateColumn()
  public deletedAt: Date | null;

  @OneToMany(() => Booking, (booking) => booking.rate)
  public bookings: Booking[];
}
