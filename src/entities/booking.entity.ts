import { Min } from 'class-validator';

import {
  Column,
  Entity,
  JoinColumn,
  ManyToMany,
  ManyToOne,
  OneToMany,
} from 'typeorm';

import { BaseEntity } from './base.entity';
import { Guest } from './guest.entity';
import { Property } from './property.entity';
import { Rate } from './rate.entity';
import { Correction } from './correction.entity';
import { BookingAmenity } from './booking-amenity.entity';

import { MAX_BOOKING_COMMENT_LENGTH, BookingStatus } from '../declarations';

@Entity({ name: 'bookings' })
export class Booking extends BaseEntity {
  @Column({ type: 'enum', enum: BookingStatus })
  public status: BookingStatus;

  @Column({
    nullable: false,
    type: 'decimal',
    precision: 10,
    scale: 2,
  })
  public totalProfit: string | number;

  @Column({
    nullable: false,
    type: 'decimal',
    precision: 10,
    scale: 2,
    comment: 'Static value of rate profit to prevent it from changes',
  })
  public appliedRateProfit: string | number;

  @Column({ type: 'timestamptz', nullable: false })
  public startDate: string;

  @Column({ type: 'timestamptz', nullable: false })
  public endDate: string;

  @Column({ type: 'int', nullable: false })
  @Min(1)
  public adults: number;

  @Column({ type: 'int', nullable: false })
  @Min(0)
  public children: number;

  @Column({
    type: 'varchar',
    length: MAX_BOOKING_COMMENT_LENGTH,
    default: null,
    nullable: true,
  })
  public comment: string | null;

  @Column({ type: 'int' })
  public propertyId: number;

  @Column({ type: 'int' })
  public rateId: number;

  @Column({ type: 'int' })
  public guestId: number;

  @ManyToOne(() => Property, (property) => property.bookings, {
    nullable: false,
  })
  @JoinColumn({ name: 'propertyId' })
  public property: Property;

  @ManyToOne(() => Rate, (rate) => rate.bookings, { nullable: false })
  @JoinColumn({ name: 'rateId' })
  public rate: Rate;

  @ManyToOne(() => Guest, (guest) => guest.bookings, { nullable: false })
  @JoinColumn({ name: 'guestId' })
  public guest: Guest;

  @OneToMany(() => Correction, (correction) => correction.booking)
  public corrections: Correction[];

  @ManyToMany(() => BookingAmenity, (bookingAmenity) => bookingAmenity.booking)
  public bookingsAmenities: BookingAmenity[];

  public isBooked() {
    return this.status === BookingStatus.BOOKED;
  }

  public isActive() {
    return this.status === BookingStatus.ACTIVE;
  }

  public isDone() {
    return this.status === BookingStatus.DONE;
  }

  public isCancelled() {
    return this.status === BookingStatus.CANCELLED;
  }
}
