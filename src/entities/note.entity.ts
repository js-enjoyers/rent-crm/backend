import { Column, Entity, JoinColumn, ManyToOne } from 'typeorm';

import { BaseEntity } from './base.entity';
import { Guest } from './guest.entity';

import { MAX_GUEST_NOTE_LENGTH } from '../declarations';

@Entity({ name: 'notes' })
export class Note extends BaseEntity {
  @Column({
    type: 'varchar',
    length: MAX_GUEST_NOTE_LENGTH,
    nullable: false,
  })
  public text: string;

  @Column({ type: 'int' })
  public guestId: number;

  @ManyToOne(() => Guest, (guest) => guest.notes, { nullable: false })
  @JoinColumn({ name: 'guestId' })
  public guest: Guest;
}
