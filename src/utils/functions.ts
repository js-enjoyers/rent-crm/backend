const isDefined = (value: unknown) => value !== undefined;

const deepTrim = (str: string) => {
  return str ? str.replaceAll(' ', '') : str;
};

const applyOmit = <T extends object, K extends keyof T>(
  entity: T,
  omitKeys: K[],
): Omit<T, K> => {
  const result: Partial<T> = { ...entity };
  omitKeys.forEach((key) => delete result[key]);

  return result as Omit<T, K>;
};

const parseProfit = (profit: number) => {
  const numberValue = parseFloat(String(profit));
  return parseFloat(numberValue.toFixed(2));
};

export { isDefined, deepTrim, applyOmit, parseProfit };
