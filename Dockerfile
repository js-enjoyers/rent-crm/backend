FROM node:20.13.1-alpine

RUN npm install -g pnpm

WORKDIR /usr/src/app

COPY package.json pnpm-lock.yaml ./

RUN pnpm install

COPY . .

RUN pnpm run build

EXPOSE $APP_PORT

CMD ["pnpm", "run", "prod"]